import {createReducer} from '@reduxjs/toolkit';
import {FETCH_WEATHER,FETCH_LOCATIONS}  from '../constants/ActionConstant';

const initialState = {
    forecastDays: [],
    locations: [],
    loadingForecastDays: false
};

const weatherReducer = createReducer(initialState, {
    [FETCH_WEATHER]: (state, action) => {
        state.forecastDays = action.payload.forecastDays;
        state.loadingForecastDays = action.payload.loadingForecastDays;
    },
    [FETCH_LOCATIONS]: (state, action) => {
        state.locations = action.payload.locations;
    }
});

export default weatherReducer;
