export const parseLocationsSelectItem = (locations) => {
    return locations.map(location => {
        return {
            id: location.woeid, text: location.title
        }
    });
}

export const getDayInWeek = (datetime) => {
    const days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    const date = new Date(datetime);

    return days[date.getDay()];
};