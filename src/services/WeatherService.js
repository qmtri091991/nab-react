import RestClientService from "./RestClientService";
import * as Endpoint from "../constants/Endpoint";
import * as RestUtil from "../constants/RestUtil";

export const fetchLocations = async (search) => {
    //Fetch data
    const response = await RestClientService.getInstance().request(
        RestUtil.REQUEST_GET,
        Endpoint.WEATHER_API_GET_LOCATION + `?query=${search}`);

    return response.data.data;
};

export const fetchWeathers = async (locationId) => {
    //Fetch data
    const response = await RestClientService.getInstance().request(
        RestUtil.REQUEST_GET,
        Endpoint.WEATHER_API_GET_WEATHER + `?woeid=${locationId}`);

    return response.data.data?.consolidated_weather;
};