import axios from 'axios';
import * as RestUtil from '../constants/RestUtil';
import * as Endpoint from '../constants/Endpoint';

export default class RestClientService {
  static instance = null;

  constructor() {
    this.baseurl = Endpoint.WEATHER_API_HOST;
  }

  static getInstance() {
    return this.instance === null
      ? (this.instance = new RestClientService())
      : this.instance;
  }

  addHeader(value) {
    this.header = Object.assign({}, this.header, value);
    return this;
  }

  removeHeader(key) {
    delete this.header[key];
    return this;
  }

  async request(
    method = RestUtil.REQUEST_GET,
    endpoint = null,
    requestParams = {}
  ) {
    this.addHeader({
    });

    const axiosInstance = axios.create({
      baseURL: this.baseurl,
      timeout: RestUtil.REQUEST_TIMEOUT,
      headers: this.header,
    });

    let dataResponse;
    let errorMessage;

    try {
      const response =
        method === RestUtil.REQUEST_GET
          ? await axiosInstance[method](endpoint, {params: requestParams})
          : await axiosInstance[method](endpoint, requestParams);

      dataResponse = response;
    } catch (e) {
      errorMessage = e.toString();
    }

    if (errorMessage) {
      throw errorMessage;
    }
    return dataResponse;
  }

}
