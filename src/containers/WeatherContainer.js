import React, {useState} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as WeatherAction from '../actions/WeatherAction';
import MasterView from '../components/common/views/MasterView';
import WeatherComponent from '../components/weather/WeatherComponent';

const WeatherContainer = (props) => {
    const [value, setValue] = useState('');

    const handleInputChange = (event, {newValue}) => {
        setValue(newValue);
    };

    const getSuggestionValue = (suggestionValue) => {
        suggestionValue && props.fetchWeatherAction(suggestionValue.id);
        return suggestionValue.text;
    };

    const onSuggestionsFetchRequested = async ({value}) => {
        await props.fetchLocationsAction(value);
    };

    const renderSuggestion = suggestion => (
        <div>
            {suggestion.text}
        </div>
    );

    // TODO: implement later
    const onSuggestionsClearRequested = () => {};

    const inputProps = {
        placeholder: 'Search location',
        value,
        onChange: handleInputChange
    };

    return (
        <MasterView>
            <WeatherComponent renderSuggestion={renderSuggestion}
                              onSuggestionsClearRequested={onSuggestionsClearRequested}
                              onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                              inputProps={inputProps}
                              suggestions={props.locations}
                              getSuggestionValue={getSuggestionValue}
                              forecastDays={props.forecastDays}
                              loadingForecastDays={props.loadingForecastDays}
            />
        </MasterView>
    )
};

const mapStateToProps = state => ({
    locations: state.weatherReducer.locations,
    forecastDays: state.weatherReducer.forecastDays,
    loadingForecastDays: state.weatherReducer.loadingForecastDays
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        Object.assign({}, WeatherAction),
        dispatch
    )
};

export default connect(mapStateToProps, mapDispatchToProps)(WeatherContainer)
