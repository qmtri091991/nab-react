import React from "react";
import {Route, Switch} from "react-router-dom";
import WeatherContainer from '../containers/WeatherContainer';

const Root = () => {
    return (
        <Switch>
            <Route path={"/"} component={WeatherContainer}/>
            <Route component={WeatherContainer}/>
        </Switch>
    );
};

export default Root;
