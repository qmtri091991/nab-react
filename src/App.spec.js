import React from 'react';
import renderer from 'react-test-renderer';
// import 'jest-styled-components'

import WeatherItemComponent from '../src/components/weather/WeatherItemComponent';
import WeatherSearchResultComponent from '../src/components/weather/WeatherSearchResultComponent';

import WeatherReducer from './reducers/WeatherReducer';
import {FETCH_WEATHER, FETCH_LOCATIONS} from "./constants/ActionConstant";


const forecastDay1 = {
    applicable_date: '2020-09-22',
    max_temp: '25.5',
    min_temp: '23.025',
};

const forecastDay2 = {
    applicable_date: '2020-09-23',
    max_temp: '25.5',
    min_temp: '23.025',
};

const forecastDay3 = {
    applicable_date: '2020-09-24',
    max_temp: '25.5',
    min_temp: '23.025',
};

const forecastDays = [forecastDay1, forecastDay2, forecastDay3];

const location1 = {
    id: 123,
    text: 'ho chi minh city'
};

const location2 = {
    id: 345,
    text: 'ha noi'
};

const locations = [location1, location2];

describe('Weather Card Item', () => {
    test('should render weather item ', () => {
        const component = renderer.create(<WeatherItemComponent forecastDay={forecastDay1} />);
        let item = component.toJSON();
        expect(item).toMatchSnapshot();
    });
});

describe('Weather Search Result', () => {
    test('should render weather fetching result ', () => {
        const component = renderer.create(<WeatherSearchResultComponent loadingForecastDays={true} forecastDays={[]} />);
        let list = component.toJSON();
        expect(list).toMatchSnapshot();
    });

    test('should render weather fetched result', () => {
        const component = renderer.create(<WeatherSearchResultComponent loadingForecastDays={false} forecastDays={forecastDays} />);
        let list = component.toJSON();
        expect(list).toMatchSnapshot();
    });
});

describe('Reducer', () => {
    it('should set forecast days', () => {
        const state = {forecastDays: [], locations: [], loadingForecastDays: false};
        const testLoadingForecastDays = false;

        const payload = {
            forecastDays,
            loadingForecastDays: testLoadingForecastDays
        };

        const newState = WeatherReducer(state, {
            type: FETCH_WEATHER,
            payload
        });

        expect(newState).toEqual({forecastDays, locations: [], loadingForecastDays: false});
    });

    it('should set locations', () => {
        const state = {forecastDays: [], locations: [], loadingForecastDays: false};
        const testLoadingForecastDays = false;

        const payload = {
            locations,
            loadingForecastDays: testLoadingForecastDays
        };

        const newState = WeatherReducer(state, {
            type: FETCH_LOCATIONS,
            payload
        });

        expect(newState).toEqual({forecastDays: [], locations, loadingForecastDays: false});
    });
});
