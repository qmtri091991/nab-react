import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

const MasterView = ({children}) => {
    return (
        <main className={'main'}>
            {children}
        </main>
    )
};

export default MasterView;
