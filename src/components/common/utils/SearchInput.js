import React from 'react';
import Autosuggest from 'react-autosuggest';

const SearchInput = ({renderSuggestion, suggestions, getSuggestionValue, inputProps, onSuggestionsFetchRequested, onSuggestionsClearRequested}) => {
    return (
        <div>
            <Autosuggest
                renderSuggestion={renderSuggestion}
                suggestions={suggestions}
                onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                onSuggestionsClearRequested={onSuggestionsClearRequested}
                getSuggestionValue={getSuggestionValue}
                inputProps={inputProps}
            />
        </div>
    )
};

export default SearchInput;