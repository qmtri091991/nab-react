import React from 'react';
import '../../styles/WeatherStyle.scss'
import WeatherSearchResultComponent from './WeatherSearchResultComponent';
import SearchInput from '../common/utils/SearchInput';

const WeatherComponent = ({choseCity, forecastDays, loadingForecastDays, ...searchInputProps}) => {
    return (
            <div className={'container-fluid wrapper'}>
                <SearchInput {...searchInputProps} />
                <WeatherSearchResultComponent loadingForecastDays={loadingForecastDays} forecastDays={forecastDays} />
            </div>
        )
    }
;

export default WeatherComponent;
