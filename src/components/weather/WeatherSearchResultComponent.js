import React from 'react';
import '../../styles/WeatherStyle.scss'
import {ListGroup, Spinner} from 'react-bootstrap';
import WeatherItemComponent from './WeatherItemComponent';

const WeatherSearchResultComponent = ({loadingForecastDays, forecastDays}) => {
        return (
            <div className={'align-middle'}>
                <ListGroup horizontal>
                    {
                        loadingForecastDays
                            ? (<><Spinner animation="grow" variant="info" /><Spinner animation="grow" variant="info" /><Spinner animation="grow" variant="info" /></>)
                            : forecastDays.map((forecastDay, idx) => <ListGroup.Item key={idx}><WeatherItemComponent
                                                                                                  forecastDay={forecastDay}/></ListGroup.Item>)
                    }
                </ListGroup>
            </div>
        )
    }
;

export default WeatherSearchResultComponent;
