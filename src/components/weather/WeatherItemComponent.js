import React from 'react';
import '../../styles/WeatherStyle.scss';
import {Card} from 'react-bootstrap';
import * as WeatherUtils from '../../utils/WeatherUtils';

const WeatherItemComponent = ({forecastDay}) => {
        return (
            <Card style={{ width: '16rem' }}>
                <Card.Img variant="top" src="https://llv.edu.vn/media/2018/01/Weather-v2-1-500x281.jpg" />
                <Card.Body>
                    <Card.Title>{WeatherUtils.getDayInWeek(forecastDay.applicable_date)}</Card.Title>
                    <Card.Text>
                        Min temp: {Math.round(forecastDay.min_temp)}°C
                    </Card.Text>
                    <Card.Text>
                        Max temp: {Math.round(forecastDay.max_temp)}°C
                    </Card.Text>
                </Card.Body>
            </Card>
        )
    }
;

export default WeatherItemComponent;
