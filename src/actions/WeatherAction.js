import * as Constants from "../constants/ActionConstant";
import {fetchLocations, fetchWeathers} from '../services/WeatherService';
import * as WeatherUtils from '../utils/WeatherUtils';

export const fetchLocationsAction = (search) => {
    return async (dispatch) => {
        const locations = await fetchLocations(search);
        const decoratedLocations = WeatherUtils.parseLocationsSelectItem(locations);
        dispatch({type: Constants.FETCH_LOCATIONS, payload: {locations: decoratedLocations}})
    }
};

export const fetchWeatherAction = (locationId) => {
    return async (dispatch) => {
        dispatch({type: Constants.FETCH_WEATHER, payload: {forecastDays: [], loadingForecastDays: true}});
        const forecastDays = await fetchWeathers(locationId);
        dispatch({type: Constants.FETCH_WEATHER, payload: {forecastDays: forecastDays, loadingForecastDays: false}})
    }
};