const ResponseUtil = require('../utils/ResponseUtil');
const RestClientService = require('../services/RestClientService');

exports.getLocations = async (req, res) => {
    let message = '';

    try {
        const query = req.query.query;
        const response = await RestClientService.createInstance().get(`https://www.metaweather.com/api/location/search?query=${query}`);
        message = ResponseUtil.formatMessage(ResponseUtil.SUCCESS_CODE, ResponseUtil.SUCCESS_MESSAGE, response.data);

        return res.json(message);
    } catch (e) {
        message = ResponseUtil.formatMessage(ResponseUtil.FAIL_CODE, ResponseUtil.FAIL_MESSAGE);
        return res.json(message);
    }
};

exports.getWeathersFromLocation = async (req, res) => {
    let message = '';

    try {
        const woeid = req.query.woeid;
        const response = await RestClientService.createInstance().get(`https://www.metaweather.com/api/location/${woeid}/`);
        message = ResponseUtil.formatMessage(ResponseUtil.SUCCESS_CODE, ResponseUtil.SUCCESS_MESSAGE, response.data);

        return res.json(message);
    } catch (e) {
        message = ResponseUtil.formatMessage(ResponseUtil.FAIL_CODE, ResponseUtil.FAIL_MESSAGE);
        return res.json(message);
    }
};