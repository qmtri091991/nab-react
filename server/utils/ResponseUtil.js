exports.SUCCESS_CODE = 200;
exports.SUCCESS_MESSAGE = 'success';
exports.FAIL_CODE = 400;
exports.FAIL_MESSAGE = 'fail';

exports.formatMessage = (status, message, data) => {
    return {
        status: status,
        message: message,
        data: data || null
    };
};