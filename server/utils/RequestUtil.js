exports.GET_REQUEST = 'get';
exports.POST_REQUEST = 'post';
exports.PUT_REQUEST = 'put';
exports.DELETE_REQUEST = 'delete';
exports.TIMEOUT = 60;