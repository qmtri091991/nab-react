'use strict';

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const WeatherController = require('./controllers/WeatherController');

// Constants
const PORT = 8000;
const HOST = '0.0.0.0';
const app = express();

app.use(cors())
app.use(bodyParser());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/api/locations', (req, res) => {
    WeatherController.getLocations(req, res);
});

app.get('/api/weathers', (req, res) => {
    WeatherController.getWeathersFromLocation(req, res);
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
