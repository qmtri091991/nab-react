const axios = require('axios');
const RequestUtil = require('../utils/RequestUtil');

let instance = null;

function RestClientService() {
    this.headers = {
        'Authorization': ''
    };

    this.timeout = RequestUtil.TIMEOUT;
}

RestClientService.createInstance = function () {
    if (!instance) {
        instance = new RestClientService();
    }

    return instance;
};

RestClientService.prototype.addHeader = function (header) {
    this.headers = Object.assign(this.headers, header);
    return this;
};

RestClientService.prototype.get = function (url, query = {}) {
    return axios.get(url, {
        params: query,
        headers: this.headers,
        timeout: 1000 * this.timeout
    })
};

RestClientService.prototype.post = function (url, params = {}) {
    return axios.post(
        url,
        params,
        {
            headers: this.headers,
            timeout: 1000 * this.timeout
        }
    );
};

module.exports = RestClientService;
