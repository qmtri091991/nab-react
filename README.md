This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start-server`

Runs the server application<br />
[http://127.0.0.1:8000](http://127.0.0.1:8000) <br />
Due to the requirement of CORS, so I implement a middle server for forwarding the request and response from https://www.metaweather.com/ <br />
And client from localhost can make requests easier.
I am using 2 API from metaweather:

/api/location/search/ -> this is used for getting suggested location<br />
/api/location/(woeid)/ -> this is used for getting the weather days from the location id<br />


### `yarn dev`

Runs the app in the development mode.<br />
Open [https://127.0.0.1](https://127.0.0.1) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Runs test.<br />

## Guideline

Go to root project <br />
Open 2 terminal <br />
Run yarn to install the necessary libraries <br />
One terminal -> Start server: yarn start-server<br />
One terminal -> Start client: yarn dev <br />
Open browser -> open [https://127.0.0.1](https://127.0.0.1) <br />
